package message;

public interface EmailService {
    public boolean isAbailable();
    public void sendEmail(String message);
}
