package message;

public interface PigeonService {
    public boolean isAvailable();
    public void sendMessage(String message);
}
