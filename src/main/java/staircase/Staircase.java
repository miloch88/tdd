/*
https://www.dailycodingproblem.com/blog/staircase-problem/
 */

package staircase;

public class Staircase {
    public static void main(String[] args) {

        System.out.println(ways(5, new int[]{1, 3, 5}));

    }

    public static int ways(int a, int[] steps) {
        return fibonacci(a + 1,  steps);
    }

    private static int fibonacci(int i, int[] steps) {

        if (i <= 0) {
            return 0;
        } else if (i == 1) {
            return 1;
        } else {

        int wynik = 0;
            for (Integer integer : steps) {
            wynik += fibonacci(i - integer, steps) ;
            }

            return wynik;
        }
    }

//    public static int ways(int a) {
//        return fibonacci(a + 1);
//    }
//
//    private static int fibonacci(int i) {
//        if (i == 0) {
//            return 0;
//        } else if (i == 1) {
//            return 1;
//        } else {
//            return fibonacci(i - 1) + fibonacci(i - 2);
//        }
//    }


}
