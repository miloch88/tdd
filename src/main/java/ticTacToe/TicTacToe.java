package ticTacToe;

public class TicTacToe {

    private char[][] board;

    public TicTacToe() {
    }

    public boolean createBoard() {
        board = new char[3][3];
        return true;
    }

    public boolean isEmpty() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] != 0) {
                    return false;
                }
            }
        }
        return true;
    }

    public void addX(int a, int b) {
        if (board[a][b] == 0) {
            board[a][b] = 'x';
        } else {
            System.out.println("Field is occupied");
        }
    }

    public void addO(int a, int b) {
        if (board[a][b] == 0) {
            board[a][b] = 'o';
        } else {
            throw new IllegalArgumentException("\"Field [%d][%d]is occupied\", a, b");
        }
    }

    public char getValue(int a, int b) {
        return board[a][b];
    }

    public boolean checkWinnerX() {
        if (
                board[0][0] == 'x' && board[0][1] == 'x' && board[0][2] == 'x' ||
                board[1][0] == 'x' && board[1][1] == 'x' && board[1][2] == 'x' ||
                board[2][0] == 'x' && board[2][1] == 'x' && board[2][2] == 'x' ||

                board[1][0] == 'x' && board[2][0] == 'x' && board[3][0] == 'x' ||
                board[1][1] == 'x' && board[2][1] == 'x' && board[3][1] == 'x' ||
                board[1][3] == 'x' && board[2][3] == 'x' && board[3][3] == 'x' ||

                board[0][0] == 'x' && board[1][1] == 'x' && board[3][3] == 'x' ||
                board[3][1] == 'x' && board[1][1] == 'x' && board[1][3] == 'x'
        ){
           return true;
        }else{
        return false;

        }
    }

}
