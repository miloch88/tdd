package secondCalculator;

public class Main {
    public static void main(String[] args) {

        SecondCalculator secondCalculator = new SecondCalculator();

        String expression = "13 15 + DUP 10 + POP 30 -";
        System.out.println("expected: " + 2 + "; was: " + secondCalculator.calculate(expression.split(" ")));

    }
}
