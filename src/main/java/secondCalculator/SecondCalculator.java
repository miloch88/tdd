package secondCalculator;


import java.util.Stack;

import static java.lang.Character.isDigit;

public class SecondCalculator {

    public SecondCalculator() {
    }

    public int calculate(String[] strings) {

        Stack<Integer> stack = new Stack<Integer>();

        for (int i = 0; i < strings.length; i++) {

            if (strings[i].equals("+")) {
                stack.push(stack.pop() + stack.pop());
            } else if (strings[i].equals("-")) {
                int a = stack.pop();
                int b = stack.pop();
                int c = a-b;
                if(c < 0){
                    return -1;
                }else {
                stack.push(c);
                }
            } else if (strings[i].equals("/")) {
                stack.push(stack.pop() / stack.pop());
            } else if (strings[i].equals("*")) {
                stack.push(stack.pop() * stack.pop());
            }else if(strings[i].equals("DUP")){
                stack.push(stack.peek());
            }else if(strings[i].equals("POP")){
                stack.pop();
            }

                else{
                stack.push(Integer.parseInt(strings[i]));
            }

        }

        if (stack.isEmpty()) {
            return -1;
        } else {
            return stack.pop();
        }
    }
}

