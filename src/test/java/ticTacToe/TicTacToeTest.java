package ticTacToe;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TicTacToeTest {

//    Nie powinnyśmy używać bo stworzy klasę jeden raz i testy nie będą niezależne

//    private static TicTacToe ticTacToe;
//
//    @BeforeClass
//    public static void setup() {
//        ticTacToe = new TicTacToe();
//        ticTacToe.createBoard();
//    }

//    Testy będą zależne bo klasa będzie tworzona za każdym razem

    private TicTacToe ticTacToe;

    @Before
    public void setup() {
        ticTacToe = new TicTacToe();
        ticTacToe.createBoard();
    }

    @Test
    public void createNewBoardTest() {
        Assert.assertTrue(ticTacToe.createBoard()); //Junit
        Assertions.assertThat(ticTacToe.createBoard()).isEqualTo(true); //AssertJ
    }

    @Test
    public void boardIsEmptyTest() {
        //        ticTacToe.createBoard();
        Assert.assertTrue(ticTacToe.isEmpty());
        Assertions.assertThat(ticTacToe.isEmpty()).isEqualTo(true);
    }

    @Test
    public void boardIsEmptyWithOneMove(){
        ticTacToe.addO(0,0);
        Assertions.assertThat(ticTacToe.isEmpty()).isEqualTo(false);
    }

//  Jeżeli użyjemy z @BeforeClass testAddX() i testAddO() jest wykonywane alfabetycznie;

    @Test
    public void testAddX() {
        // Możemy to dodać w konstruktorze @BeforeClass
//        ticTacToe.createBoard();
        ticTacToe.addX(2, 2);
        Assert.assertEquals('x', ticTacToe.getValue(2, 2));
    }

    @Test
    public void testAddO() {
        ticTacToe.addO(2, 2);
        Assert.assertEquals('o', ticTacToe.getValue(2, 2));
    }

    @Test (expected = IllegalArgumentException.class)
    public  void  addTwoTimesTheSameMoveTestExpected(){
        ticTacToe.addO(2, 2);
        ticTacToe.addO(2, 2);
    }

    @Test
    public  void  addTwoTimesTheSameMoveTestTryCatch(){
        try{
        ticTacToe.addO(2, 2);
        ticTacToe.addO(2, 2);

        // Nie jest potrzebne bo obsługujemy to w TicTacToe
        Assert.fail("Should appear IllegalArgumentException");
        } catch(IllegalArgumentException e){
            Assert.assertEquals(e.getMessage(), "\"Field [%d][%d]is occupied\", a, b");
        }
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void addMoveWithWrongCooridination(){
        ticTacToe.addO(4,4);
//        Assert.assertEquals('o',ticTacToe.getValue(4,4));
    }

    @Test
    public void whoWin(){
        ticTacToe.addX(0,0);
        ticTacToe.addX(0,1);
        ticTacToe.addX(0,2);
        Assertions.assertThat(ticTacToe.checkWinner()).isEqualTo(true);

    }
}
