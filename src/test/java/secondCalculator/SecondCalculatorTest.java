package secondCalculator;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class SecondCalculatorTest {

    private static SecondCalculator secondCalculator;

    @BeforeClass
    public static void setup(){ secondCalculator = new SecondCalculator();}

    @Test
    public void emptyStack(){
        int result = secondCalculator.calculate(new String[0]);
        Assert.assertEquals(-1,result);
    }

    @Test
    public void addElement(){
        int result = secondCalculator.calculate(new String[]{"13"});
        Assert.assertEquals(13, result);
    }

    @Test
    public void addSecondElement(){
        int result = secondCalculator.calculate(new String[]{"13","15"});
        Assert.assertEquals(15, result);
    }

    @Test
    public void calculateAdd(){
        int result = secondCalculator.calculate(new String[]{"13", "15", "+"});
        Assert.assertEquals(28, result);
    }

    @Test
    public void calculateSubtract(){
        int result = secondCalculator.calculate(new String[]{"13", "15", "-"});
        Assert.assertEquals(2, result);
    }

    @Test
    public void calculateMultiply(){
        int result = secondCalculator.calculate(new String[]{"13", "15", "*"});
        Assert.assertEquals(195, result);
    }

    @Test
    public void calculateDivide(){
        int result = secondCalculator.calculate(new String[]{"13", "15", "/"});
        Assert.assertEquals(1, result);
    }

    @Test
    public void duplicateTest(){
        int result = secondCalculator.calculate(new String[]{"13", "15", "+", "DUP"});
        Assert.assertEquals(28, result);
    }

    @Test
    public void secondTestAdd(){
        int result = secondCalculator.calculate(new String[]{"13", "15", "+", "DUP", "10", "+"});
        Assert.assertEquals(38, result);
    }

    @Test
    public void popTest(){
        int result = secondCalculator.calculate(new String[]{"13", "15", "+", "DUP", "10", "+", "POP"});
        Assert.assertEquals(28, result);
    }

    @Test
    public void finalTest(){
        int result = secondCalculator.calculate(new String[]{"13", "15", "+", "DUP", "10", "+", "POP", "30", "-"});
        Assert.assertEquals(2, result);
    }

    @Test
    public void resultIsLessZero(){
        int result = secondCalculator.calculate(new String[]{"35", "10", "-"});
        Assert.assertEquals(-1, result);
    }

    
}
