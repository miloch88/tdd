package message;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class NotificationServiceTest {

    //mockujemy pierwszy Interface
    @Mock
    EmailService emailService;
    //Mockujemy drugi Interface
    @Mock
    PigeonService pigeonService;
    //Wrzucamy mocki do naszej testowanej klasy
    @InjectMocks
    NotificationService notificationService;

    String string = "abc";

    @Test
    public void whenEmailIsAvailable(){
        //mówimy mu jakie wartośći ma przybrać 
        when(emailService.isAbailable()).thenReturn(true);

        notificationService.sendNotification(string);

       verify(emailService).isAbailable();
       verify(emailService).sendEmail(string);
    }

    @Test
    public void whenEmailIsUnavailableAndPigeonIsAvailable(){
        when(emailService.isAbailable()).thenReturn(false);
        when(pigeonService.isAvailable()).thenReturn(true);

        notificationService.sendNotification(string);

        verify(emailService).isAbailable();
        verify(pigeonService).isAvailable();
        verify(emailService, never()).sendEmail(string);
        verify(pigeonService).sendMessage(string);
    }

    @Test
    public void whenEmailAndPigeonAreAvailable(){
        when(emailService.isAbailable()).thenReturn(false);
        when(pigeonService.isAvailable()).thenReturn(false);

        notificationService.sendNotification(string);

        verify(emailService).isAbailable();
        verify(pigeonService).isAvailable();
        verify(emailService, never()).sendEmail(string);
        verify(pigeonService, never()).sendMessage(string);
    }

}
