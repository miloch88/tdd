package tokenizer;

import org.assertj.core.api.Assertions;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

public class TokenizerTest {

    private static Tokenizer tokenizer;

    @BeforeClass
    public static void setup(){
        tokenizer = new Tokenizer();
    }

    @Test
    public void testByAssertJNull(){
        Assertions.assertThat(tokenizer.doSomting(null)).isNotNull().isEmpty();
    }

    @Test
    public void testByAssertJEmpty(){
        Assertions.assertThat(tokenizer.doSomting("")).isNotNull().isEmpty();
    }

    @Test
    public void testByAssertJAbc(){
        Assertions.assertThat(tokenizer.doSomting("abc")).isNotNull().isEmpty();
    }

    @Test
    public void testByAssertJOneZero(){
        Assertions.assertThat(tokenizer.doSomting("1.0")).hasSize(1).contains(1.0);
    }

    @Test
    public void testByAssertJOnePlusOne(){
        Assertions.assertThat(tokenizer.doSomting("1+1")).hasSize(3).contains(1.0,1.0,2.0);
    }
}
