package calculator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

@RunWith(value = Parameterized.class)
public class KonstruktorTest {

    private Calculator calculator;
    private int a;
    private int b;
    private Integer expected;

    //Określamy miejsce parametru przez konstrukor

    public KonstruktorTest(int a, int b, int expected) {
        this.a = a;
        this.b = b;
        this.expected = expected;
    }

    //Jeżeli chodzi o następne działania to muszę utworzyć nową klasę testową

    @Parameterized.Parameters(name = "{index}: testAdd {0}+{1} = {2}")
    public static Iterable<? extends Object> data() {
        return Arrays.asList(
                new Object[][]{
                        {1, 1, 2},
                        {3, 3, 6},
                        {3, 7, 10},
                });
    }

    @Before
    public void setup() {
        calculator = new Calculator();
    }

    @Test
    public void testAdd() {
        Assert.assertEquals(expected, calculator.add(a, b));
    }
}
