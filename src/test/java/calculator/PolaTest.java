package calculator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

@RunWith(value = Parameterized.class)
public class PolaTest {

    //Określamy miesce parametru przez pole

    private Calculator calculator;
    @Parameterized.Parameter(value = 0)
    public int a;
    @Parameterized.Parameter(value = 1)
    public int b;
    @Parameterized.Parameter(value =2)
    public Integer expected;


    //Jeżeli chodzi o następne działania to muszę utworzyć nową klasę testową

    @Parameterized.Parameters(name = "{index}: testAdd {0}+{1} = {2}")
    public static Iterable<? extends Object> data(){
        return Arrays.asList(
                new Object[] []{
                        {1,1,2},
                        {3,3,6},
                        {3,7,10},
                });
    }

    @Before
    public  void setup(){
        calculator = new Calculator();
    }

    @Test
    public void testAdd(){
        Assert.assertEquals(expected,calculator.add(a,b));
    }
}


