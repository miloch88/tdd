package calculator;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(JUnitParamsRunner.class)
public class JUnitParams {

    private Calculator calculator;

    @Before
    public void setup(){
        calculator = new Calculator();
    }

    @Test
    @Parameters({
            "15,7,8",
            "1,12,-11",
            "2,2,0"})
    public void substractTest(int a, int b, int expected){
        int result = calculator.subtraction(a,b);
        Assert.assertEquals(expected,result);
    }

    @Test
    //@Parameters muszą być zaimportowane z biblioteki JUnitParams!!!!
    @Parameters({
            "15,7,22",
            "1,12,13",
            "2,2,4"})
    public void addTest(int a, int b, int expected){
        int result = calculator.add(a,b);
        Assert.assertEquals(expected,result);
    }
}
