package calculator;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CalculatorTest {

    private static Calculator calculator;

    // Utworzy klasę za każdym razem
    // @Before

    //Utworzy klasę raz
    @BeforeClass
    public static void setup(){
        calculator = new Calculator();
    }

    @Test
    public void addTest(){
        int result = calculator.add(2,3);

        Assert.assertEquals (5,result);
    }

    @Test
    public void addTwoNegativeNumbers(){
        int result = calculator.add(-1,-4);
        Assert.assertEquals(-5, result);
    }

    @Test
    public void addTwoZeros(){
        int result = calculator.add(0,0);
        Assert.assertEquals(0, result);
    }

    //Wykluczają się wzajemnie divideByZero i arithmeticException
    @Test
    public void divideByZero(){
        try{
        int result = calculator.divide(1,0);

            //Wyświetla jeżli wyjątek się nie wyświetli
            Assert.fail("Lipa ");

        }catch (IllegalArgumentException e){
            Assert.assertEquals(e.getMessage(), "Niedziel przez zero");

        }
    }

    //Wykluczają się wzajemnie divideByZero i arithmeticException
    @Test(expected = ArithmeticException.class)
    public void arithmeticExceptionTest(){
        calculator.divide(2,0);
    }

    @Test
    public void assertJDivide(){
        int result = calculator.divide(null,3);
        Assertions.assertThat(result).as("sdfsafsa").isEqualTo(2).isGreaterThan(0).isNull();
    }


}
