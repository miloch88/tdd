package middleValue;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class MiddleValueTest {

    private static MiddleValue middleValue;

    @BeforeClass
    public static void start(){
        middleValue = new MiddleValue();
    }

    @Test
    public  void emptyArrayMiddle(){
        int result = middleValue.returnMiddle(new int[0]);
        Assert.assertEquals(-1, result);
    }

    @Test
    public void oneElelemtArrayMiddle(){
        int result = middleValue.returnMiddle(new int[]{5});
        Assert.assertEquals(5, result);
    }

    @Test
    public void oddArrayMiddle(){
        int result = middleValue.returnMiddle(new int[]{5, 2, 1, 4, 3 , 7 , 6});
        Assert.assertEquals(4, result);
    }

    @Test
    public void evenArrayMiddle(){
        int result = middleValue.returnMiddle(new int[]{6, 1, 2, 5, 3, 4});
        Assert.assertEquals(3   , result);
    }
}
